var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			GALLERIES
	\*----------------------------------------------------------------*/
	$('.gallery-items .gallery-item').featherlightGallery({
		previousIcon: '<svg id="arrowhead-left" viewBox="0 0 64 64"><path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M40 44.006L26 32.012l14-12.006" stroke-linejoin="round" stroke-linecap="round"></path></svg>',
		nextIcon: '<svg id="arrowhead-right" viewBox="0 0 64 64"><path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linejoin="round" stroke-linecap="round"></path></svg>',
		galleryFadeIn: 100,
		galleryFadeOut: 300
	});
});