var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			ICON SLIDERS
	\*----------------------------------------------------------------*/
	if ($('.icon-glide .glide').length) {
		new Glide('.icon-glide .glide', {
			type: 'carousel',
			startAt: 0,
			perView: 5,
			focusAt: 'center',
			gap: 25,
			autoplay: 3000,
			breakpoints: {
				800: {
					perView: 3,
				},
				500: {
					perView: 1,
				}
			}
		}).mount()
	}
});