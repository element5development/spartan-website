var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		INPUT ADDING AND REMVOING CLASSES
	\*----------------------------------------------------------------*/
	$('input:not([type=checkbox]):not([type=radio])').focus(function () {
		$(this).addClass('is-activated');
	});
	$('textarea').focus(function () {
		$(this).addClass('is-activated');
	});
	$('select').focus(function () {
		$(this).addClass('is-activated');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*------------------------------------------------------------------
		HAMBURGER
	------------------------------------------------------------------*/
	$(".hamburger-menu").click(function () {
		$(this).toggleClass("is-active");
		$('.navigation-block nav').toggleClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	SEARCH
	\*----------------------------------------------------------------*/
	$("#search-op").click(function () {
		$(".search").addClass("is-active");
	});
	$(".search-open").click(function () {
		$(".search").addClass("is-active");
	});
	$("#search-close").click(function () {
		$(".search").removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
		COOKIE POP UP
	\*----------------------------------------------------------------*/
	var visited = $.cookie('visited');
	if (visited == 'yes') {
		$('.cookie-use').css('display', 'none');
	} else {
		// do nothing
	}
	$.cookie('visited', 'yes', {
		expires: 1 // the number of days cookie  will be effective
	});
	$(".cookie-use button").click(function () {
		$('.cookie-use').addClass("is-hidden");
	});
	/*----------------------------------------------------------------*\
  	ACCORDION
	\*----------------------------------------------------------------*/
	$(".question").click(function () {
		$(this).toggleClass("is-active");
	});

});