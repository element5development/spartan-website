<?php 
/*----------------------------------------------------------------*\

	COOKIE USE POP UP

\*----------------------------------------------------------------*/
?>

<?php 
	$post_url = get_permalink();
	$post_content = get_the_excerpt();
	$featured_img = get_home_url() . get_the_post_thumbnail_url();
	$post_title = get_the_title();
?>

<section class="cookie-use">
	<div class="block">
		<p>Our site uses cookies. By continuing to use our site you are agreeing to our <a href="/privacy-policy/">cookie policy</a>.</p>
		<button class="is-primary">Accept and Close</button>
	</div>
</section>