<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>
<section class="faq-contact">
	<div class="section is-large-width has-large-spacing">
		<h2>Speak with a Spartan Wealth Advisor</h2>
		<div class="flex-contain">
			<?php if (  have_rows('questions', 285) && 'publish' === get_post_status(285) ) : ?>
				<div class="faq">
					<h3>How can we help you?</h3>
					<?php $i = 0; while ( have_rows('questions', 285) ) : the_row(); $i++; ?>
						<?php if ( $i < 7 ) : ?>
							<div class="question">
								<p><?php the_sub_field('question'); ?></p>
								<svg>
									<use xlink:href="#arrowhead-down" />
								</svg>
								<span class="answer">
									<?php the_sub_field('answer'); ?>
								</span>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
			<div class="contact">
				<?php echo do_shortcode('[gravityform id="6" title="false" description="false"]'); ?>
			</div>
		</div>
	</section>
</section>
<footer class="page-footer">
	<div class="section is-large-width has-standard-spacing">
		<div class="logo">
			<?php dynamic_sidebar('footer-one'); ?>
		</div>
		<div class="social">
			<?php dynamic_sidebar('footer-two'); ?>
		</div>
		<div class="nav">
			<?php dynamic_sidebar('footer-three'); ?>
		</div>
		<div class="life-insurance">
			<?php dynamic_sidebar('footer-four'); ?>
		</div>
		<div class="brokercheck">
			<?php dynamic_sidebar('footer-five'); ?>
		</div>
		<div class="copyright">
			<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</nav>
		</div>
		<div class="stock-ticker">
			<?php dynamic_sidebar('footer-six'); ?>
		</div>
	</div>
</footer>