<?php 
/*----------------------------------------------------------------*\

	SIMPLE FOOTER
	Used commonly on landing pages

\*----------------------------------------------------------------*/
?>


<footer class="page-footer">
	<div class="section is-large-width has-standard-spacing">
		<div class="logo">
			<?php dynamic_sidebar('footer-one'); ?>
		</div>
		<div class="social">
			<?php dynamic_sidebar('footer-two'); ?>
		</div>
		<div class="nav">
			<?php dynamic_sidebar('footer-three'); ?>
		</div>
		<div class="life-insurance">
			<?php dynamic_sidebar('footer-four'); ?>
		</div>
		<div class="brokercheck">
			<?php dynamic_sidebar('footer-five'); ?>
		</div>
		<div class="copyright">
			<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</nav>
		</div>
		<div class="stock-ticker">
			<?php dynamic_sidebar('footer-six'); ?>
		</div>
	</div>
</footer>