<?php 
/*----------------------------------------------------------------*\

	LEFT SIDEBAR
	Used on the sidebar-both and sidebar-left templates

\*----------------------------------------------------------------*/
?>

<aside class="is-left">
	<?php dynamic_sidebar('left'); ?>
</aside>