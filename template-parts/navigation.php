<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<nav class="top-navigation-block">
	<?php wp_nav_menu(array( 'theme_location' => 'top_nav' )); ?>
</nav>
<div class="navigation-block">
	<a href="<?php echo get_home_url(); ?>">
		<svg>
			<use xlink:href="#logo" />
		</svg>
	</a>
	<div class="wrap">
		<div class="search-open">
			<svg>
				<use xlink:href="#search-open" />
			</svg>
		</div>
		<div class="hamburger-menu">
			<svg>
				<use xlink:href="#menu" />
			</svg>
			<svg>
				<use xlink:href="#close" />
			</svg>
		</div>
	</div>
	<nav>
		<a href="tel:+13132542112" class="phone"> (313) 254-2112 </a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
		<?php wp_nav_menu(array( 'theme_location' => 'top_nav' )); ?>
		<div class="social">
			<?php dynamic_sidebar('footer-two'); ?>
		</div>
		<span id="search-op">
			<svg>
				<use xlink:href="#search-open" />
			</svg>
		</span>
	</nav>
</div>
<section class="search">
	<span id="search-close">
		<svg>
			<use xlink:href="#close-white" />
		</svg>
	</span>
	<?php echo get_search_form(); ?>
</section>