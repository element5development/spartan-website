<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
	<section class="block is-large-width has-standard-spacing">

		<h1>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				else :
					the_title();
				endif;
			?>
		</h1>

		<?php if ( get_field('button') ) : ?>
			<?php $button = get_field('button'); ?>
			<div class="buttons">
				<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
					<?php echo $button['title']; ?>
				</a>
			</div>
		<?php endif; ?>

	</section>

	<?php if( have_rows('links') ): ?>
		<section class="links">
			<?php while ( have_rows('links') ) : the_row(); ?>
				<div class="link">
					<a href="<?php the_sub_field('url'); ?>">
						<h2>
							<?php the_sub_field('title'); ?>
						</h2>
						<p>
							<?php the_sub_field('description'); ?>
						</p>
						<div class="overlay"></div>
					</a>
				</div>
			<?php endwhile; ?>
		</section>
	<?php endif; ?>

	<div class="overlay"></div>

</header>