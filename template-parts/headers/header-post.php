<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<?php 
	$banner = get_field('banner_image');
	$full_banner = $banner['sizes']['xlarge'];
?>

<header class="page-title has-image" style="background-image: url('<?php echo $full_banner; ?>');">
	<section class="block is-standard-width has-standard-spacing">

		<?php 
			if ( get_field('position') ) : 
				$class = 'has-subheader';
			endif; 
		?>
		<h1 class="<?php echo $class; ?>"><?php 
				if ( get_field('display_name') ) :
					the_field('display_name');
				else :
					the_title();
				endif;
			?>
		</h1>

		<?php if ( get_field('position') ) : ?>
			<p class="subheader"><?php 
				the_field('position'); ?>
			</p>
		<?php endif; ?>

		<?php if ( get_field('linkedin_profile') ) : ?>
			<div class="contact-buttons">
					<a target="_blank" href="<?php the_field('linkedin_profile'); ?>" class="button is-icon is-ghost is-borderless is-small">
						<svg>
							<use xlink:href="#linkedin"></use>
						</svg>
					</a>
			</div>
		<?php endif; ?>

		<?php if ( get_field('broker_check_url') ) : ?>
			<a href="<?php the_field('broker_check_url'); ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri()  ?>/dist/images/broker-check.png" alt="BrokerCheck" />
			</a>
		<?php endif; ?>

	</section>

	<div class="overlay"></div>

</header>