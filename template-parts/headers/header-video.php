<?php 
/*----------------------------------------------------------------*\

	HEADER WITH VIDEO BACKGROUND

\*----------------------------------------------------------------*/
?>

<header class="page-title has-video" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
	<section class="block is-standard-width has-standard-spacing">

		<?php 
			if ( get_field('title_description') ) : 
				$class = 'has-subheader';
			endif; 
		?>
		<h1 class="<?php echo $class; ?>">
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				else :
					the_title();
				endif;
			?>
		</h1>

		<?php if ( get_field('title_description') ) : ?>
			<p class="subheader">
				<?php the_field('title_description'); ?>
			</p>
		<?php endif; ?>

		<?php if ( have_rows('title_buttons') ) : $i = 1; ?>
			<div class="buttons">
				<?php while( have_rows('title_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<?php if ( $i == 2 ) :
						$class = 'is-primary is-ghost';
					else :
						$class = 'is-primary';
					endif; ?>
					<a class="button <?php echo $class; ?>" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php $i++; endwhile; ?>
			</div>
		<?php endif; ?>

	</section>

	<div class="overlay"></div>

	<video muted="" autoplay="" loop="" poster="" class="bgvid"> 
		<source src="<?php the_field('title_bg_vid'); ?>" type="video/mp4"> 
	</video>

</header>