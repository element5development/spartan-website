<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types and search results

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');

	if ( $posttype == '' ) {
		$posttype = 'blog';
	}

	if ( is_search() ) :
		$image = "";
	else : 
		$image = "background-image: url('" . get_field( $posttype . '_title_bg_img', 'option') . "');";
	endif;
?>

<header class="page-title has-image" style="<?php echo $image; ?>">
	<section class="block is-standard-width has-standard-spacing">

		<?php if ( is_search() ) : ?>
			<h1 class="has-subheader">Search</h1>
			<p class="subheader">Showing results for <?php echo get_search_query(); ?></p>
		<?php else : ?>
		<h1><?php the_field( $posttype . '_page_title', 'option'); ?></h1>
			<?php if ( get_field( $posttype . '_title_description', 'option') ) : ?>
				<p class="subheader"><?php the_field( $posttype . '_title_description', 'option'); ?></p>
			<?php endif; ?>
		<?php endif; ?>

	</section>

	<div class="overlay"></div>
</header>

<?php if ( is_home() || is_category() ) : ?>
	<nav class="categories">
		<ul>
			<?php wp_list_categories( array('title_li' => '')); ?>
		</ul>
	</nav>
<?php endif; ?>