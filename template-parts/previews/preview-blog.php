<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="chip">
		<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>
	</div>
	<h1><?php the_title(); ?></h1>
	<div class="featured-image has-overlay">
		<?php $image = get_field('banner_image'); ?>
		<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<p class="subheader"><?php echo get_excerpt(150); ?></p>
	<div class="buttons">
		<div class="button is-ghost">Read More</div>
	</div>
	<a href="<?php the_permalink(); ?>"></a>
</article>
