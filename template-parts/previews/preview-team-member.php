<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR TEAM MEMBERS

\*----------------------------------------------------------------*/
?>

<?php 
	$headshot = get_field('headshot');
	$small_headshot = $headshot['sizes']['medium'];
?>

<article class="preview preview-team" style="background-image: url(<?php echo $small_headshot; ?>);">
	<a href="<?php the_permalink(); ?>"></a>
	<div class="info">
		<a href="<?php the_permalink(); ?>">
			<h3><?php the_field('display_name'); ?></h3>
		</a>
		<p><?php the_field('position'); ?></p>
		<div class="contact-buttons">
			<?php if ( get_field('phone_number') ) : ?>
				<?php 
					$phone = preg_replace('/\D/', '', get_field('phone_number'));
				?>
				<a href="tel:+1<?php echo $phone; ?>" class="button is-icon is-ghost is-borderless is-small">
					<svg>
						<use xlink:href="#teamphone"></use>
					</svg>
				</a>
			<?php endif; ?>
			<?php if ( get_field('email') ) : ?>
				<a href="mailto:<?php the_field('email'); ?>" class="button is-icon is-ghost is-borderless is-small">
					<svg>
						<use xlink:href="#teamemail"></use>
					</svg>
				</a>
			<?php endif; ?>
			<?php if ( get_field('linkedin_profile') ) : ?>
				<a target="_blank" href="<?php the_field('linkedin_profile'); ?>" class="button is-icon is-ghost is-borderless is-small">
					<svg>
						<use xlink:href="#teamlinkedin"></use>
					</svg>
				</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="overlay"></div>
</article>
