<?php 
/*----------------------------------------------------------------*\

	IMAGE GALLERY WITH LIGHTBOX ZOOM

\*----------------------------------------------------------------*/
?>

<div class="gallery">
	<section class="is-standard-width has-standard-spacing">
		<?php if ( get_field('gallery_title') ) : ?>
			<h2 class="has-subheader"><?php the_field('gallery_title'); ?></h2>
		<?php endif; ?>
		<?php if ( get_field('gallery_description') ) : ?>
			<p class="subheader"><?php the_field('gallery_description'); ?></p>
		<?php endif; ?>
		<div class="gallery-items">
			<?php 
				$images = get_field('gallery');
			?>
			<?php foreach( $images as $image ): ?>
				<a class="gallery-item" href="<?php echo $image['url']; ?>">
					<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			<?php endforeach; ?>
		</div>
	</section>
</div>