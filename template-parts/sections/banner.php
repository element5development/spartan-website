<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>


<div class="banner" style="background-image: url('<?php the_field('banner_bg_img'); ?>');">
	<section class="is-standard-width has-large-spacing">

		<?php if ( get_field('banner_title') ) : ?>
			<h2 class="has-subheader"><?php the_field('banner_title'); ?></h2>
		<?php endif; ?>

		<?php if ( get_field('banner_description') ) : ?>
			<p class="subheader"><?php the_field('banner_description'); ?></p>
		<?php endif; ?>

		<?php if ( have_rows('banner_buttons') ) : $i = 1; ?>
			<div class="buttons">
				<?php while( have_rows('banner_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<a class="button is-primary" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php $i++; endwhile; ?>
			</div>
		<?php endif; ?>

	</section>
</div>