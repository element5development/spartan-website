<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED POSTS BY CATEGORY

\*----------------------------------------------------------------*/
?>

<?php
	$categories = get_the_category();
	$category_id = $categories[0]->cat_ID;
?>

<section class="related-posts has-standard-spacing">
	<h3>Related Posts</h3>
	<div class="grid section is-large-width has-two-column">
		<?php
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 2,
				'order' => 'ASC',
				'post__not_in' => array( $post->ID ),
				'cat' => $category_id,
			);
			$loop = new WP_Query( $args );
			?>
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php get_template_part( 'template-parts/previews/preview-blog' ); ?>
			<?php endwhile;?>
			<?php wp_reset_query(); ?>
	</div>
</section>