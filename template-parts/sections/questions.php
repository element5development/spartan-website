<?php 
/*----------------------------------------------------------------*\

	QUESTION CARDS

\*----------------------------------------------------------------*/
?>

<section class="questions is-standard-width has-large-spacing">
	<?php while ( have_rows('questions') ) : the_row(); ?>

		<div class="question">
			<h2><?php the_sub_field('question'); ?></h2>
			<?php the_sub_field('answer'); ?>
		</div>

	<?php endwhile; ?>
</section>
