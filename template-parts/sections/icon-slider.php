<?php 
/*----------------------------------------------------------------*\

	SLIDER FOR ICONS 
	commonly used for awards and partners

\*----------------------------------------------------------------*/
?>

<div class="icon-glide">
	<section class="is-large-width has-standard-spacing">
		<?php if ( get_field('icon_slider_title', 'option') ) : ?>
			<h2 class="has-subheader"><?php the_field('icon_slider_title', 'option'); ?></h2>
		<?php endif; ?>
		<?php if ( get_field('icon_slider_description', 'option') ) : ?>
			<p class="subheader"><?php the_field('icon_slider_description', 'option'); ?></p>
		<?php endif; ?>
		<?php if ( have_rows('icons', 'option') ) : ?>
			<div class="glide">
				<div class="glide__track" data-glide-el="track">
					<ul class="glide__slides">
						<?php while( have_rows('icons', 'option') ) : the_row(); ?>
							<?php $icon = get_sub_field('icon', 'option'); ?>
							<li class="glide__slide">
								<?php if ( get_field('url') ) : ?>
									<a target="_blank" href="<?php the_field('url'); ?>" class="block">
										<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
									</a>
								<?php else : ?>
									<div class="block">
										<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
									</div>
								<?php endif; ?>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
		<?php endif; ?>
	</section>
</div>