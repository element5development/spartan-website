<?php 
/*----------------------------------------------------------------*\

	STEPS

\*----------------------------------------------------------------*/
?>


<section class="steps grid has-three-column is-standard-width has-large-spacing">
	<?php while ( have_rows('steps') ) : the_row(); ?>
		<div class="step">
			<?php 
				$image = get_sub_field('image'); 
				$small_img = $image['sizes']['small'];
			?>
			<img src="<?php echo $small_img; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php the_sub_field('description'); ?>
		</div>
	<?php endwhile; ?>
</section>