<?php 
/*----------------------------------------------------------------*\

	CARDS

\*----------------------------------------------------------------*/
?>


<section class="cards is-standard-width has-large-spacing">

	<?php if ( get_field('cards_title') ) : ?>
		<h2><?php the_field('cards_title') ?></h2>
	<?php endif; ?>

	<div class="grid has-three-column">
		<?php while ( have_rows('cards') ) : the_row(); ?>
			<?php if (  get_sub_field('icon') ) : //FLIP CARD ?> 
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<?php 
								$image = get_sub_field('icon'); 
								$small_img = $image['sizes']['small'];
							?>
							<img src="<?php echo $small_img; ?>" alt="<?php echo $image['alt']; ?>" />
							<p><?php the_sub_field('title'); ?></p>
						</div>
						<div class="back">
							<h3><?php the_sub_field('description'); ?></h3>
							<?php if ( get_sub_field('button') ) : ?>
								<?php $link = get_sub_field('button'); ?>
								<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="button"><?php echo $link['title']; ?></a>
							<?php endif; ?>	
						</div>
					</div>
				</div>
			<?php else : //NORMAL CARD?>
				<div class="card-container">
					<div class="card">
						<div class="side">
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php the_sub_field('description'); ?></p>
							<?php if ( get_sub_field('button') ) : ?>
								<?php $link = get_sub_field('button'); ?>
								<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="button"><?php echo $link['title']; ?></a>
							<?php endif; ?>	
						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
</section>