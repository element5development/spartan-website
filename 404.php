<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<section class="main-content-block is-standard-width has-standard-spacing">
				<h1 class="has-subheader">Oops!</h1>
				<p class="subheader">We can't seem to find the page you're looking for.</p>
				<div class="buttons">
					<a class="button is-primary" href="/">Return to the Homepage</a>
					<a class="button is-primary is-ghost" href="/contact/">Contact Support</a>
				</div>
			</section>
		</article>
	</main>

	<?php get_template_part('template-parts/footers/footer-simple'); ?>

</div>

<?php get_footer(); ?>