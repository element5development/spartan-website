<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php get_template_part('template-parts/headers/header-front'); ?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
		<?php if( have_rows('cards', 284) ): ?>
			<section class="cards is-standard-width has-large-spacing">
				<h2>What's important to you?</h2>
				<div class="grid has-three-column">
					<?php while ( have_rows('cards', 284) ) : the_row(); ?>
					<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
						<div class="flipper">
							<div class="front">
								<?php 
									$image = get_sub_field('icon'); 
									$small_img = $image['sizes']['small'];
								?>
								<img src="<?php echo $small_img; ?>" alt="<?php echo $image['alt']; ?>" />
								<p><?php the_sub_field('title'); ?></p>
							</div>
							<div class="back">
								<h3><?php the_sub_field('description'); ?></h3>
								<?php if ( get_sub_field('button') ) : ?>
									<?php $link = get_sub_field('button'); ?>
									<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="button"><?php echo $link['title']; ?></a>
								<?php endif; ?>	
							</div>
						</div>
					</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<section class="contact-simple">
			<div class="section is-standard-width has-standard-spacing">
				<h2>Contact Us</h2>
				<?php echo do_shortcode('[gravityform id="7" title="false" description="false"]'); ?>
			</div>
		</section>
	</main>

	<?php get_template_part('template-parts/footers/footer-simple'); ?>

</div>

<?php get_footer(); ?>