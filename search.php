<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<?php get_template_part('template-parts/headers/header-archives'); ?>

<main>
	<a id="content" class="anchor"></a>
	<?php if ( have_posts() ) : ?>
		<section class="feed feed-search default-contents is-standard-width has-large-spacing">
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>" class="result preview">
					<h2>
						<?php 
							if ( get_field('page_title') ) :
								the_field('page_title');
							else :
								the_title();
							endif;
						?>
					</h2>
					<?php the_excerpt(); ?>
				</a>
			<?php endwhile; ?>
		</section>
		<section class="infinite-scroll is-standard-width has-small-spacing">
			<div class="page-load-status">
				<p class="infinite-scroll-request">
					<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
						<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
							<animateTransform attributeType="xml"
								attributeName="transform"
								type="rotate"
								from="0 25 25"
								to="360 25 25"
								dur="0.6s"
								repeatCount="indefinite"/>
						</path>
					</svg>
				</p>
				<p class="infinite-scroll-last"></p>
				<p class="infinite-scroll-error"></p>
			</div>
			<?php the_posts_pagination( array(
				'prev_text'	=> __( 'Previous page' ),
				'next_text'	=> __( 'Next page' ),
			) ); ?>
			<a class="load-more button">Load more</a>
		</section>
	<?php else : ?>
		<section class="feed feed-search default-contents is-standard-width has-large-spacing">
			<div class="result preview">
				<h2>No Results Found. Try again.</h2>
				<form role="search" method="get" action="<?php echo get_site_url(); ?>">
					<input type="search" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					<button type="submit">Search</button>
				</form>
			</div>
		</section>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/footers/footer'); ?>

<?php get_footer(); ?>