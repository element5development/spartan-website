<?php 
/*----------------------------------------------------------------*\

	SINGLE TEAM MEMBER TEMPLATE

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php get_template_part('template-parts/headers/header-post'); ?>

	<main>
		<a id="content" class="anchor"></a>
		
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-small-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
			<div class="contact-buttons">
				<?php 
					$first_name = explode(' ',trim(get_field('display_name')));
				?>
				<?php if ( get_field('email') ) : ?>
					<a href="mailto:<?php the_field('email'); ?>" class="button is-massive">Email <?php echo $first_name[0]; ?></a>
				<?php endif; ?>
				<?php 
					$phone = preg_replace('/\D/', '', get_field('phone_number'));
				?>
				<?php if ( get_field('phone_number') ) : ?>
					<a href="tel:+1<?php echo $phone; ?>" class="button is-primary is-massive">Call <?php echo $first_name[0]; ?></a>
				<?php endif; ?>
			</div>
		</article>
		<?php 
			$previous = get_previous_post();
			$next = get_next_post();
		?>
		<section class="links has-small-spacing">
			<div class="section is-standard-width">
				<div class="previous">
					<?php if ( get_next_post() ) : ?>
						<a href="<?php the_permalink($next); ?>">
							<svg>
								<use xlink:href="#arrowhead-left" />
							</svg>
							<?php echo the_field('display_name', $next); ?>
						</a>
					<?php endif; ?>
				</div>
				<div class="next">
					<?php if ( get_previous_post() ) : ?>
						<a href="<?php the_permalink($previous); ?>">
							<?php echo the_field('display_name', $previous); ?>
							<svg>
								<use xlink:href="#arrowhead-right" />
							</svg>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</section>
	</main>

	<?php 
		get_template_part('template-parts/footers/footer-simple');
	?>

</div>

<?php get_footer(); ?>