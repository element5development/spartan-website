<?php 
/*----------------------------------------------------------------*\

	DISPLAY COMMENTS
	commonly will be overridden by the WPdiscus plugin

\*----------------------------------------------------------------*/
?>

<?php if ( post_password_required() ) {
  return;
} ?>

<section class="comments-area is-standard-width has-standard-spacing">
  <?php comment_form(); ?>
</section>
