<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/

function left_sidebar() {
	$args = array(
		'name'          => __( 'Left Sidebar' ),
		'id'            => 'left',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'left_sidebar' );

function right_sidebar() {
	$args = array(
		'name'          => __( 'Right Sidebar' ),
		'id'            => 'right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'right_sidebar' );

function footer_one() {
	$args = array(
		'name'          => __( 'Footer Area One' ),
		'id'            => 'footer-one',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_one' );

function footer_two() {
	$args = array(
		'name'          => __( 'Footer Area Two' ),
		'id'            => 'footer-two',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_two' );

function footer_three() {
	$args = array(
		'name'          => __( 'Footer Area Three' ),
		'id'            => 'footer-three',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_three' );

function footer_four() {
	$args = array(
		'name'          => __( 'Footer Area Four' ),
		'id'            => 'footer-four',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_four' );

function footer_five() {
	$args = array(
		'name'          => __( 'Footer Area Five' ),
		'id'            => 'footer-five',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_five' );

function footer_six() {
	$args = array(
		'name'          => __( 'Footer Area Six' ),
		'id'            => 'footer-six',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_six' );