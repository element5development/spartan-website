<?php

/*----------------------------------------------------------------*\
	BUTTON SHOTCODE
\*----------------------------------------------------------------*/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
    'url' => '#',
		'type' => '',
		'size' => '',
  ), $atts ) );
  $link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.' is-'.$size.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode('btn', 'cta_button');