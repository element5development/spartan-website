<?php

/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", create_function("", "return false;"));

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	CHANGE SUBMIT INPUTS TO BUTTONS
\*----------------------------------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );

/*----------------------------------------------------------------*\
	UPDATE URL INPUTS TO INCLUDE HTTP
\*----------------------------------------------------------------*/
function check_website_field_value( $form ) {
	foreach ( $form['fields'] as &$field ) {  
		if ( 'website' == $field['type'] || ( isset( $field['inputType'] ) && 'website' == $field['inputType']) ) {  
			$value = RGFormsModel::get_field_value($field);  
			if (! empty($value) ) { 
				$field_id = $field['id']; 
				if (! preg_match("~^(?:f|ht)tps?://~i", $value) ) {  
					$value = "http://" . $value;
				}
				$_POST['input_' . $field_id] = $value; 
			}
		}
	}
	return $form;
}
add_filter( 'gform_pre_render', 'check_website_field_value' );
add_filter( 'gform_pre_validation', 'check_website_field_value' );

/*----------------------------------------------------------------*\
	PULL IN SERVICES INTO DROPDOWN
\*----------------------------------------------------------------*/
function populate_services( $form ) {
	foreach ( $form['fields'] as &$field ) {
		if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-services' ) === false ) {
			continue;
		}
		// get acf values from services page & set drop down values
		$choices = array();
		while ( have_rows('cards', 284) ) : the_row();
			$choices[] = array( 'text' => get_sub_field('description'), 'value' => get_sub_field('description') );
		endwhile;
		// update placeholder
		$field->placeholder = 'Select Service of Interest';
		$field->choices = $choices;
	}
	return $form;
}
add_filter( 'gform_pre_render_4', 'populate_services' );
add_filter( 'gform_pre_validation_4', 'populate_services' );
add_filter( 'gform_pre_submission_filter_4', 'populate_services' );
add_filter( 'gform_admin_pre_render_4', 'populate_services' );

/*----------------------------------------------------------------*\
	PULL IN OPENINGS INTO DROPDOWN
\*----------------------------------------------------------------*/
function populate_posts( $form ) {
	foreach ( $form['fields'] as &$field ) {
		if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
			continue;
		}
		// get acf values from careers page & set drop down values
		$choices = array();
		while ( have_rows('cards', 288) ) : the_row();
			$choices[] = array( 'text' => get_sub_field('title'), 'value' => get_sub_field('title') );
		endwhile;
		// update placeholder
		$field->placeholder = 'Select an Opening';
		$field->choices = $choices;
	}
	return $form;
}
add_filter( 'gform_pre_render_5', 'populate_posts' );
add_filter( 'gform_pre_validation_5', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_5', 'populate_posts' );
add_filter( 'gform_admin_pre_render_5', 'populate_posts' );