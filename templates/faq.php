<?php 
/*----------------------------------------------------------------*\

	Template Name: FAQ

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		else :
			get_template_part('template-parts/headers/header-image');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>

			<!-- DEFAULT WSISWIG -->
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>

			<!-- QUESTIONS -->
			<?php if ( have_rows('questions') ) :
				get_template_part('template-parts/sections/questions');
			endif; ?>

			<!-- BANNER / CTA -->
			<?php if ( get_field('banner_title') ) : 
				get_template_part('template-parts/sections/banner');
			endif; ?>

		</article>
	</main>

	<!-- FAQ / CONTACT SECTION -->

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>